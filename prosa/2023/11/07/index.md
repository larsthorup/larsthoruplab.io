---
date: 2023-11-07
layout: page-en
title: I toget
author: Lars
permahost: "https://www.larsthorup.dk"
permalink: "/prosa/2023/11/07/"
---

# I toget

Lars Thorup, 2023-11-07

Toget begynder langsomt at køre, og en kvinde kanter sig vej hen til det ledige sæde skråt over for mig. Jeg havde håbet jeg kunne få skrevet lidt, men toget er helt fuldt, og jeg tvivler på jeg får ro til det. Ham ved siden af mig har heldigvis sat sig til at sove, så jeg slipper for at føle mig læst over skulderen. Jeg kigger op og betragter hende mens hun vikler sig fri af sin jakke men beholder det røde silketørklæde om halsen. Hun minder mig om en af børnenes skolekammeraters mor, men det er ikke hende. Hun sætter sig på pladsen ved vinduet og fisker sin telefon frem. Jeg skriver tre ord og sletter dem igen og skæver over mod hende. Vi er på vej forbi Valby nu, det er lyntoget, og vi gynger alle sammen i takt hver gang skinnerne drejer en smule. Villahaverne suser forbi og bliver afløst af lagerbygninger og gamle skorstene. Hun smiler meget tænker jeg. Hun sidder nok og skriver med sin kæreste. Jeg prøver at forestille mig hvad de skriver til hinanden. Længere henne i toget sidder der en familie med børn som taler højlydt med hinanden. Eller snarere til hinanden. Hører de mon overhovedet hvad hinanden siger, eller har ungerne hørt de voksnes reprimander så mange gange at de ikke længere hører dem? Nu kigger hun ud af vinduet, men det er som om der stadig spiller et smil i hendes mundvig, glimter en stille fryd i hendes øjne, mens vi forlader Tåstrup og landskabet skifter til marker og læhegn og små skramlede bondehuse. Hendes tørklæde er gledet ned fra skulderen. Jeg sukker og samler mig igen om at skrive, og pludselig fanger papiret, ordene kommer rullende.