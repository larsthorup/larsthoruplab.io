---
date: 2023-11-14
layout: page-en
title: Radioavisen
author: Lars
permahost: "https://www.larsthorup.dk"
permalink: "/prosa/2023/11/14/"
---

# Radioavisen

Lars Thorup, 2023-11-14

jeg kommer ud i køkkenet<br>
min mor står ved køkkenbordet med ryggen til<br>
hun kaldte på mig lige før<br>
det er frokosttid og jeg kravler op på stolen ved det lille spisebord<br>
middagssolen vælter ind ad vinduerne<br>
radioen kører og som altid passer frokosten med at de spiller kirkeklokkerne fra rådhuset i københavn<br>
og så starter radioavisen<br>
jeg hører ikke efter<br>
jeg er sulten<br>
jeg sidder lidt uroligt på stolen og så vender min mor sig rundt og kommer med tallerkenen<br>
det er den samme tallerken som altid<br>
med de små kaniner rundt langs kanten<br>
den er lidt barnlig synes jeg nu<br>
rødbedeskiverne ligger sirligt på leverpostejsmaden<br>
jeg er sulten og begynder at spise og min mor aer mig over håret inden hun vender sig om og stiller sig tilbage ved køkkenbordet<br>
hun står stille og kigger ud af vinduet<br>
mon hun lytter til al den voksensnak der strømmer ud af radioavisen<br>
jeg kommer i tanke om den hule jeg var begyndt at bygge ovre i skoven i går<br>
jeg må tilbage og gøre den færdig<br>
jeg skal bare lige spise! 