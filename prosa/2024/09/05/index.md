---
date: 2024-09-05
layout: page-en
title: Skovbrynet
author: Lars
permahost: "https://www.larsthorup.dk"
permalink: "/prosa/2024/09/05/"
---

# Skovbrynet

Lars Thorup, 2024-09-05

Nu stod hun der igen, ved skovbrynet. Uskyldigt så det jo egentlig ud. Mon hun igen ville få skovfolket at se? 

Sidst havde hun selv været uskyldig, stadig et barn. Nu føltes det mere alvorligt, som noget hun måtte. Eller netop ikke måtte. Men ville. Hun bukkede sig ned og snørede skoene op og sparkede dem af. Så bandt hun hårbåndet op og slog håret ud så det viftede i den lette sommerbrise. Hun ville se ud som dem.

Hun vendte sig om mod skovbrynet. Bladhænget så jo pænt og imødekommende ud, men inde bag det lysegrønne ydre ventede mørket og den tunge lugt af fugtig skovbund, svampe og væltede træer. Og trolde måske? Hun kastede et sidste blik ud over de åbne enge bag hende. Så trak hun op i den lyse sommerkjole og trådte tøvende ind ad jordstien bag stendiget på sine bare fødder. 