---
date: 2024-09-25
layout: page-en
title: Hvis du vil noget med litteratur!
author: Lars
permahost: "https://www.larsthorup.dk"
permalink: "/prosa/2024/09/25/litteratur/"
---

# Hvis du vil noget med litteratur!

Lars Thorup, 2024-09-25

"Syntes han virkelig godt om den roman?" spørger jeg og kigger oprevet på dig. "Jeg er målløs! Hvad tænker du?" 

Vi sidder på bænken under det store lindetræ midt i skolegården. Du sidder med et finurligt smil på læben. Du kender ham selvfølgelig også meget bedre end mig.

"Altså, er der sådan en slags standard, du synes man skal leve op til?" spørger du.

"Aj, men altså!" Jeg er stadig målløs. "Han underviser gymnasieelever i litteratur!"

"Men..." Du tøver og rynker panden og kigger indgående på mig. "Synes du ikke den var underholdende?"

"Altså... joh... men, nej! Det er jo bare en bog fuld af banaliteter og sproget lød nærmest som en havnearbejder efter fyraften. Det er da for plat til at være littertur!"

"Måske skulle du prøve at spørge noget mere i stedet for bare at være fuld af dine egne småborgerlige fordomme," siger du og rejser dig op med et sæt, så din lange mørke hestehaler svinger fra side til side.

Du kaster mig et sidste blik inden du tager din taske over skulderen og går hen mod porten.

"Men... hey...! Vent!" råber jeg og kigger mig rundt for at se hvor jeg lagde min jakke og min taske.

Så skynder jeg mig efter dig. "Jeg vil gerne spørge om noget så!"

Du er nået et godt stykke ned ad den stejle gade mod havnen, før jeg indhenter dig.

"Hvorfor bliver du sur?" spørger jeg forpustet og prøver at holde takt med dig.

"Jeg har måske også en slags standard, jeg synes du burde leve op til," fnyser du. "Du bliver nødt til at være lidt mere nysgerrig, hvis du vil noget med litteratur. Eller med mig!"

"Jamen, okay, okay," siger jeg. "Jeg vil gerne forsøge. Vil du give mig en chance, måske? Kom, må jeg ikke byde dig på en øl her inde på bodegaen. Så lover jeg at jeg kun vil stille spørgsmål!"

"Men, vil du så også lytte til svarene?" spørger du og kigger sidelæns på mig. Jeg kan se dit lille finurlige smil spille i mundvigen igen og jeg ånder lettet op.