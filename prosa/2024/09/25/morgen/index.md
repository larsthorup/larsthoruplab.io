---
date: 2024-09-25
layout: page-en
title: Morgenrødme
author: Lars
permahost: "https://www.larsthorup.dk"
permalink: "/prosa/2024/09/25/morgen/"
---

# Morgenrødme

Lars Thorup, 2024-09-25

De første strejf af lyserødt bredte sig op gennem sprækkerne i regnskyerne ude over sundet. Vinden var løjet af og han hørte kun den knagende lyd når dønningerne ramte bolværket. En terne fløj lige imod ham og drejede fra i sidste øjeblik med et hæst skrig. Bænken han sad på var våd, mærkede han nu.

Hvorfor tog hun aldrig med ham herned om morgenen?
