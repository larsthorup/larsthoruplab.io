---
date: 2024-09-04
layout: page-en
title: Svend
author: Lars
permahost: "https://www.larsthorup.dk"
permalink: "/prosa/2024/09/04/"
---

# Svend

Lars Thorup, 2024-09-04

Svend står ovre i den anden ende af lokalet og rager op over de mange festklædte mennesker. Som altid er han omgivet af unge studerende. Han nyder deres opmærksom, mens han fortæller om endnu en af de mange ting, han synes han ved mere om end de fleste. Og de lapper det i sig. Jeg står egentlig og konverserer institutbestyreren, men jeg hører hele tiden hans kraftige stemme og kan ikke koncentrere mig. Jeg ved jo godt hvordan han egentlig er, hvordan han nysgerrigt og ydmygt kan åbne en diskussion med mig, ivrig efter at høre hvordan jeg ser på sagen. Hvordan kan han være så selvfed? Jeg hører en af de studerende bryde ud i latter og jeg vender mig om og ser i det sekund, hvordan hans smukke ansigt pludselig er en andens, en lille usikker dreng, der evigt frygter at blive afsløret. Som aldrig har lært noget. Som altid må charmere sig igennem. Som kan falde ned fra tronen hvert øjeblik.