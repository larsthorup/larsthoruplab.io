---
date: 2024-09-29
layout: page-en
title: Frej og Lilja
author: Lars
permahost: "https://www.larsthorup.dk"
permalink: "/prosa/2024/09/29/"
---

# Frej og Lilja

Lars Thorup, 2024-09-29

Frej strammer basten og stiller neget op ved siden af de andre, der står og støtter mod hinanden. Havrens klokker nikker i den friske vind og skal nok tørre hurtigt. Han kigger ud mod horisonten og konstarerer tilfreds, at de mørke skyer snarere driver nordover end ind mod dem. Han får øje på Maren, som står lidt væk og laver det samme som ham. Han er egentlig stadig lidt lun på hende. De havde flirtet så fint dér sidste år til høstballet. De havde danset polka, og han havde kysset hende på kinden under fuldmånen. Men nu var hun blevet forlovet med Bjarke, den mest tumpede af herremandens sønner. Frej sukker. Maren havde ikke været til at komme i nærheden af siden. Hun drømte nok bare om den sølle slatarv, som Bjarke kunne gøre sig berettiget til. Han sætter sig på en af de store sten, som de ikke har orket at grave fri, og tænker tilbage.

Efter forlovelsen i foråret var han løbet sin vej. Han havde tænkt, at han ville løbe over til nabobyen og søge arbejde der, så han slap for at møde Maren hele tiden. Snart var han drejet ind i skoven, som han ellers holdt sig fra. Det lignede ham ikke at gøre den slags skøre ting. Enhver normal ung mand ville holde sig væk fra skoven. Og snart var han et godt stykke oppe ad en skovklædt skråning, hvorfra han stadig kunne skimte tilbage mod markerne, han kom fra. Lidt senere stod han i kanten af en lysning og blev opdaget af de tre høje skovfolk, der sad rundt om bålet. De inviterede ham til at spise med. Han forstod ikke meget af deres tale, det lød som om de sang med bløde og pludrende stemmer. De grinede ad ham og delte ud af den lækre nystegte kanin. Og så tilbød kvinden med det lange lyse hår og de levende blå øjne at følge ham videre gennem skoven, indtil han igen kunne finde vej videre til den næste by.

"Altså, Frej, havren samler sig jo ikke i neg af sig selv," hører han pludselig fatter sige med træt stemme. Han har slet ikke set ham komme. Frej rejser sig op og venter på, at faderen får klaget sig færdig. Han synes det er uretfærdigt, for han er effektiv når han arbejder. Og enhver har vel lov til en pause. Tænk, hvis han en dag kunne få lidt påskønnelse. 

Frej trækker kasketten ned i panden og river videre. Hun hed Lilja, husker han tydeligt.

<center>. . . o o o O O O o o o . . .</center>

Lilja roder i sin bæltepung. Der er både fyrsvamp, en bennål, alt for meget snor, et par tørrede rosenknopper. Men dér er den! Hun læner sig tilbage mod den tykke egestamme og giver sig til at gnaske på den korte stump af lakridsrod. Her oppe fra har hun god udsigt over den lille lysning, og hun har før haft held til at spotte vildsvin her. Hun checker endnu engang sin slynge, som virker spændstig og solid. Hun nyder den sødt krydrede smag af lakrids og tænker på Baldur, som hun havde vundet den fra forleden. Hun lukker øjnene og griber instinktivt om en tynd gren som sikkerhed, inden hun kort efter falder i søvn.

"Hej!," lyder der et råb, og hun vågner med et sæt. "Ligger du nu og sover igen!"

Lilja er lige ved at falde ned, men grenen holder heldigvis. Hun kigger ned og ser Eskild stå grinende for foden af træet.

"Hvad så?" råber hun tilbage, let irriteret over at blive forstyrret. "Jeg sov altså ikke, jeg jager vildsvin!"

"Ja, det et godt med dig," siger Eskild. "Vil du ikke hellere med ud at fiske? Der kommer ikke nogen vildsvin forbi foreløbig, er jeg sikker på."

"Det ved du vel ikke meget om," svarer Lilja og ryster kroppen vågen. "Du har da ikke fanget nogen i umindelige tider."

"Vil du med ud at fiske?" spørger Eskild igen.

"Ja, så pyt da," overgiver Lilja sig, egentlig glad ved tanken om at lave noget sammen med Eskild i stedet for bare at sidde og kede sig her i træet alene.

Hun kravler ned og står snart ved siden af ham.

"Hvor er fiskegrejet?" spørger hun, da det går op for hende at Eskild er tomhændet.

"Ja, øh, det skal vi lige hente på vejen," svarer Eskild og giver sig til at lunte i retning af deres lejr.

"Hvad? Din løgnhals!" griner Lilja og sætter i løb efter ham. "Du sagde, du havde det med!"

De løber med lette, hastige skridt ud og ind mellem træerne efter hinanden med deres lange hår virvlende i vinden. Eskild er hurtig, men Lilja har længere ben og haler ind på ham. Snart når hun op på siden af ham og får et ben ind under hans bagerste ben, og Eskild tumler fremover og falder så lang han er i den bløde skovbund.

"Aj, helt ærligt!" når Eskild at råbe, inden Lilja er over ham, og snart ruller de rundt.

Lilja får drejet ham rundt på ryggen og presser hans skuldre mod jorden, mens hun sidder stakåndet på knæ ind over ham. Han presser imod, men hun er stærkere end ham, og til sidst giver han op og sænker skuldrene og lader kroppen falde slapt sammen på jorden under hende. Lilja bøjer sig ned over ham. Hendes lange lyse hår falder ned på begge sider af hendes ansigt. Hun kigger ind i hans mørkebrune øjne under de sorte krøller. 

"Jeg tror, jeg ved, hvad du fisker efter," ler hun og lægger sig ned oven på ham og kysser ham.