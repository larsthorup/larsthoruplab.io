---
date: 2024-12-24
layout: page-en
title: Når man går to på fjeldet
author: Lars
permahost: "https://www.larsthorup.dk"
permalink: "/prosa/2024/12/24/"
---

# Når man går to på fjeldet

Lars Thorup, 2024-12-24

Granernes grene hænger mørkt over os, dækket i store puder af tung sne. En barket træstamme har klemt sig bag om et større klippestykke, der nu danner en fin lille bænk til os. Far har børstet sneen af klippen og vi sidder på vores vanter og svinger de trætte ben frit i luften. Skiene har vi stukket ned i en snedrive et par meter væk og far har åbnet rygsækken og fisker lækkerierne op. 

Madpakken består af en klapsammen med myseost og en anden med elgpølse, begge på det tørre norske brød af sammalt hvete. Efter de mange kilometer løjpe smager det fortrinligt. Vi siger ingenting til hinanden mens vi spiser. Far sidder og skuer rundt i lysningen ind mellem granerne. Jeg sidder og tænker på hvor langt der er hjem. Da vi har spist, kommer termokanden frem og far fylder koppen med skoldhed solbærsaft, og vi skiftes til at nippe af den, inden den bliver kold.

"Ssshhh," siger far og kigger ind mellem granerne.

Ingen af os har sagt en lyd, men jeg tier alligevel endnu mere stille og prøver at kigge i samme retning. Og nu ser jeg en bevægelse, og kort efter kommer hjorten gående med forsigtige skridt. Den må løfte benene godt i den høje sne, og den kaster hele tiden blikket rundt til alle sider, før den tager et par skridt mere. Et stykke bag den kommer der én til, lidt mindre end den første. De er nok tyve meter væk, og jeg kan tydeligt se deres vagtsomme øjne og sorte snuder.

"Ssshhh," hvisker far igen næsten uhørligt.

Vi sidder helt stille, mens de to hjorte langsomt går forbi langs kanten af lysningen, til de forsvinder ind mellem de sneklædte graner og ned bag skråningen.

"Jeg tror ikke, de så os," siger far stille og kigger på mig.

"De var tæt på!" udbryder jeg begejstret.

"Vi var heldige med vinden," siger far. "Den blæser hen mod os, så de kunne nok ikke lugte os."

"Var det dens unge?" spørger jeg. "Den anden?"

"Det var i hvert fald en unge," siger far. "Måske hendes lam fra i sommers."

"Hvorfor kalder du den 'lam'?" spørger jeg og griner. "Det er da ikke et får!"

"Ja, det hedder rådyrets unger altså," siger far. "Lam. Rålam."

Vi sidder lidt stille og drikker det sidste af den varme saft. Jeg skal tisse og hopper ned fra klippebænken og den pludselige bevægelse får en gren til at svippe så der falder en kæmpe dynge sne ned lige foran os.

"Hov," siger jeg og står helt stille, men der kommer ikke mere.

Længe daler de glitrende snekrystaller ned over os, mens skoven igen falder til ro. Jeg går lidt væk og får med besvær trukket de mange lag tøj til side så jeg kan tisse.

Så stavrer jeg tilbage på mine ømme ben og får sat mig op ved siden af far igen. Han finder den mørke plade chokolade frem og brækker en stribe af til os begge. 

"Ah, det var godt," siger far. "Synes du ikke?"

"Er der langt hjem?" spørger jeg.

"Vi er over halvvejs," siger far. "Det er jo en rundløjpe. Vi er hjemme inden det bliver mørkt. Er du træt i benene?"

"Lidt," indrømmer jeg.

"Lad os bare blive siddende lidt længere," siger far. "Så kan du lige hvile benene lidt mere."

Far tager fuglebogen frem fra tasken og giver sig til at bladre rundt og læse hist og her.

"Jeg skal lige se, hvad det var for en finke, vi så før," siger han.

Jeg læner mig tilbage mod træstammen og kigger rundt i lysningen. Jeg blinker lidt med øjnene. Der er igen noget, der bevæger sig. Jeg kniber øjnene sammen og prøver at se hvad det kan være. Måske er det en mus?

<center>. . . o o o O O O o o o . . .</center> 

Nu kan jeg se den lille grønne tophue. Jeg kniber øjnene sammen. Det er en lille fyr. Med stort skæg, men selv lillebitte. Han kommer gående på ski med en slæde bundet efter sig. Han kommer hen mod mig, og der er en til, der kommer gående på ski efter ham. Med rød tophue og røde kinder.

Jeg vinker til dem og den lille mandsling ændrer kurs og snart står de foran os. I slæden er der to bitte små børn pakket godt ind i tæpper. 

"Er I nisser?" spørger jeg, da de står lige neden for mig.

"Nisser?" udbryder den lille mandsling og slår en kæmpe latter op. "Nisser! Ja klart, det! Hvad ellers måske? Ligner vi trolde, måske? Hvad siger du lillemor, skulle vi være blevet forvandlet til elverfolk?"

"Ja, okay," siger jeg. "Jeg synes jo også I ligner nisser."

"Ja, og du ligner et menneskebarn," siger nissen. "Er I ikke langt væk hjemmefra for sådan en dreng som dig? Hvor bor I?"

"Vi bor ovre på den anden side af Birkevattnet," siger jeg. "Lige ved Mosbo. Er der langt hjem?"

"Næh, vel ikke for sådanne mennesker med jeres lange ben," siger nissen.

"Jeg er nu lidt træt," siger jeg. "Vi er gået hele vejen nord om søen. Men nu har vi lige spist og hvilet lidt."

"Nord om vattnet, siger du?" funderer nissen. "Ja, så klarer I det nok. Har I ikke nogen slæde?"

"Næh, kun vores ski," siger jeg og peger over på de fire ski, der stritter som visne grene op ad snetuen.

Jeg får øje på far, der stadig sidder og bladrer i sin bog og nu løfter kikkerten fra halsen og kigger ind mellem træerne. Jeg skal lige til at sige til ham, at vi har fået selskab.

"Du er da en sej gutte," siger nissen.

"Min far siger jeg nok skal klare det," siger jeg. "Synes du, jeg er sej? Det siger min far aldrig."

"Ja, rundt om vattnet, det er da næsten to mil," siger nissen. "Det ville tage os flere dage. Er det din far, der bestemmer, hvor langt du kan gå?"

"Næh, altså," siger jeg og undrer mig over spørgsmålet. "Altså det kan han vel ikke bestemme? Det er jeg jo bare nødt til?"

"Ej, du må vite at det er dig selv, der bestemmer hvad du kan," siger nissen. "Det kan andre aldrig bestemme over. Du må sige til din far, at du skal være med til at bestemme når I er på tur."

"Joh," siger jeg tøvende. "Jeg er jo bare en dreng."

"Når man går to på fjeldet, er man to om at bestemme," siger nissen.

"Okay," siger jeg. "Det lyder som et godt råd."

"Nissefar giver kun gode råd," ler nissemor bag ham. "Det tror han i hvert fald selv! Men denne gangen har han nu ret, det kan jeg sige."

"Nå, vi må videre," siger nissefar. "Har I fået noget at spise? Uten mat og drikke, duer helten ikke!"

"Jo, tak, vi havde en god madpakke med," siger jeg. "Med geitost og elgpølse!"

"Så greit, det!" siger nissemor. "God tur hjemover!"

Så sætter de skiene i fart igen, og jeg kigger efter dem, mens de forsvinder ind mellem træerne.

<center>. . . o o o O O O o o o . . .</center> 

"Nå min dreng, er du klar til at gå videre?"

Jeg kigger op på far med et sæt. Han kigger ned på mig med et smil.

"Så du nisserne, far?"

"Nisserne?" ler far. "Mon ikke du blundede lidt?"

"Nej, der var to nisser med en slæde. Jeg snakkede med dem. Hørte du det ikke? De er lige kørt videre den vej der."

"Joh... måske så jeg godt et eller andet, der bevægede sig den vej lige før," siger far tøvende. "Og nisser har der jo altid været her i skoven. Det må man tro."

"Jeg så dem altså," siger jeg lidt stædigt. "Og snakkede med dem."

Jeg kan godt høre på far, at han ikke rigtig tror på mig. Måske var det bare noget jeg havde drømt?

"Skal vi gå videre?" spørger far. "Så vi ikke når at blive helt kolde."

"Ja, nu kan jeg vist godt," siger jeg. "Bare vi ikke skal skynde os. Jeg vil godt gå forrest, så kan jeg bestemme hvor hurtigt vi går."

"..." far tøver inden han giver sig. "Ja, lad os sige det!"

Lidt senere har vi fået skiene på igen. Det er blevet lidt lysere, selvom solen stadig er skjult bag de lave skyer. Skyggerne gør løjpen nem at følge, og jeg kan mærke i benene, at det var godt med et hvil og noget mad.

"Det bliver godt at komme hjem til pejsen!" råber jeg bagud mod far. "Må jeg godt tænde op, når vi kommer hjem?"

"Ja, det er da klart!" råber far tilbage. 