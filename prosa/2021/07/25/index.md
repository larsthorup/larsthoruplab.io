---
date: 2021-07-25
layout: page-en
title: Johanne
author: Lars
permahost: "https://www.larsthorup.dk"
permalink: "/prosa/2021/07/25/"
---

# Johanne

Lars Thorup, 2021-07-25

"Hej igen!" siger jeg overrasket og smiler, da jeg genkender kvinden, der kommer gående hen imod mig. 

Jeg har sat mig på en bænk med en fin udsigt over en lille sø og har lige pakket min madpakke ud af rygsækken. Kvinden kommer gående ad den lille sti nede ved bredden, som jeg selv kom ad. Vi havde krydset hinanden tidligere på dagen og jeg havde lagt mærke til hende da vi begge hilste. Nu kommer hun gående hen imod mig og jeg betragter hende igen. Hun smiler, da hun får øje på mig.

"Ja, nu kan jeg da godt genkende dig," siger hun og smiler tilbage.

Hun er lille og tætbygget med tydelige former under det tætsiddende vandretøj. Hun ser ud til at være noget ældre end mig med langt mørkt hår, sat i en hestehale, der vifter fra side til side mens hun går. Snart er hun helt henne ved mig.

"Velbekomme," siger hun.

"Tak, du må gerne slå dig ned," siger jeg og rykker mig over på den ene side af bænken. "Jeg skulle til at spise min frokost."

"Ja, det kunne da være hyggeligt med lidt selskab," siger hun og løfter rygsækken af skuldrene. "Jeg har også frokost med."

Jeg kigger på hende mens hun tager termokande og en lille kasse op ad tasken. Hun har smukke fingre, solbrændte og stærke. Hun ser ud til at være i glimrende form.

"Det er sådan et smukt sted her," siger hun og åbner metalæsken og tager en lille sandwich ud.

"Kommer du her tit?" spørger jeg, mens jeg vikler elastikken af min pose med blandede nødder og rosiner.

"Njah, tit og tit," siger hun. "Men et par gange om året i hvert fald. Er det første gang du er her?"

"Ja, jeg er her på efterårsferie," siger jeg. "Har booket et værelse nede i byen, og havde set at der var gode vandreruter herude. Men du bor måske selv i nærheden så?"

"Njah, lidt uden for byen," siger hun mens hun skruer låget af termokanden og hælder varm kaffe op. "Men det er nemt at cykle hertil, så det er tit jeg kører herud og vandrer."

"Skål," siger jeg og løfter min vandflaske, og vi griner og skåler i vand og kaffe.

Snart fortæller vi ivrigt hinanden om os selv. Hun er 20 år ældre end mig og leder et team af programmører i den lokale afdeling af et større ingeniørfirma. Jeg er lige blevet ansat i mit første job som færdigguddannet programmør. Så vi har masser at snakke om. Jeg lytter fascineret mens hun fortæller hvordan hun leder sit team; ganske anderledes end dér hvor jeg selv er startet. Og hun spørger nysgerrigt til nye ting jeg har lært på studiet, og stiller dybdeborende spørgsmål til mit speciale.

Vi har nok siddet og snakket en time, og forlængst spist vores mad. Hun begynder at pakke sammen.

"Vi kan følges tilbage til parkeringspladsen," siger hun. "...tænker du måske også skal den vej?"

"Ja, lad os det," siger jeg begejstret. "Min cykel står også dér... eller altså: den cykel jeg har lånt på hotellet."

Vi går en anden vej tilbage end jeg havde tænkt mig. Det viser sig at hun kender et par interessante omveje, og vi ser mange forskellige svampe og plukker modne vilde æbler.

Da vi nærmer os parkeringspladsen, tænker jeg på om vi kan blive ved at være sammen lidt endnu. Jeg ved faktisk ikke om hun har en partner. Jeg prøver at tage mod til mig et par gange, før det endelig lykkes mig.

"Du... jeg tænkte på...," siger jeg tøvende, "...om du havde lyst til at vi sætter os et sted og drikker en øl sammen eller noget...?"

Hun drejer sig om mod mig og sender mig et stort smil.

"Ih, det vil jeg gerne!" siger hun. "Forresten hedder jeg Johanne."

"Nåh ja!" siger jeg og smiler genert tilbage. "Jeg hedder Lars. Jeg tror nok at man kan få en øl i baren på mit hotel, der kan vi vist endda sidde ude."

Så cykler vi tilbage til byen, og parkerer uden for byens vistnok eneste, men ganske hæderlige, lille hotel. Der sidder allerede et par stykker på den overdækkede veranda, som er bygget i klassisk western saloon stil.

Vi bestiller og sætter os over for hinanden ved et ledigt bord. Snart kommer tjeneren med to store glas kold fadøl.

"Skål," siger vi i kor og griner og klinker glassene sammen og drikker begge en stor slurk af den forfriskende øl.

"Ah," sukker jeg. "Det var godt."

Jeg kigger nysgerrigt på Johanne, som sætter glasset på bordet og kigger tilbage på mig. Indtil nu har vi kun siddet, gået og cyklet ved siden af hinanden, men nu sidder vi pludselig overfor hinanden og kan se hinanden i øjnene. Mit blik flakker genert, men jeg bliver ved med at kigge på hende. Et smil spiller i hendes mundvig, og hendes øjne glimter. Hendes smukke hænder piller ved den ekstra ølbrik vi fik med.

"Du ser dejlig ud," udbryder jeg inden jeg når at tænke mig om. Jeg rødmer og tænker lynhurtigt adskillige dårlige undskyldninger uden at jeg dog kan vælge en at sige.

Men Johanne sender mig bare et kæmpe smil.

"Aj, du er sød!" udbryder hun. "Du ser nu også selv godt ud!"

Jeg får lyst til at røre ved hendes hænder, som hun sidder dér og smiler til mig og drejer ølbrikken mellem sine fingre.

"Har du en kæreste?" spørger hun så.

"Njah, ikke sådan rigtig," siger jeg.

"Aj, det kan man altså ikke sige," udbryder Johanne og slår en latter op. "Enten har man en kæreste eller også har man ikke en kæreste? Eller hvad? Det er måske anderledes i vore dage?"

"Nåh, nej, men det er lidt specielt," siger jeg. "Min kæreste læser i udlandet, så vi sér ikke hinanden før til vinter. Og vi har aftalt at vi godt må være sammen med andre."

"Nåh, på den måde," siger Johanne. "Okay, det forstår jeg måske godt. Så kæresten er på pause, så at sige?"

"Ja, det kan man godt sige," siger jeg. "Hvad med dig, har du nogen kæreste? Eller en mand?"

"Næh, jeg blev skilt for mange år siden," siger Johanne. "Og jeg har heller ikke nogen kæreste for tiden."

Vi tøver begge lidt forlegne over det oplagte næste skridt. Johanne skal lige til at sige noget, men jeg får afbrudt hende ved at løfte glasset til en skål. Vi skåler og er stadig tavse og forlegne.

"Jeg går lige ind på toilettet," siger Johanne og rejser sig.

Mens hun er væk tænker jeg på hvordan jeg skal tage næste skridt. Jeg har mange idéer, og de føles alle sammen kejtede eller kluntede.

Så kommer Johanne tilbage og jeg følger hende med øjnene mens hun kanter sig hen til vores bord og sætter sig ned over for mig.

"Kan du godt lide mine bryster?" spørger hun med et listigt smil og rækker begge hænder ind over bordet mod mig. "Du kigger tit på dem, synes jeg..."

"Åh... men... jeg..." stammer jeg forfjamsket.

Så tager jeg mig sammen og rækker hænderne frem og tager hendes hænder i mine.

"Du har flotte bryster," siger jeg og kigger hende ind i øjnene, "men jeg vil altså også sige at jeg har lagt mærke til at du har nogen meget flotte hænder."

Jeg lader hendes hænder ligge i mine og glider forsigtigt min tommelfinger hen over hendes fingre.

"Aj, du er altså sød," siger Johanne. "Det er jeg glad for du synes! Jeg kan også selv godt lide mine hænder."

"Kan du også godt selv lide dine bryster?" spørger jeg lidt for kæphøjt nu hvor jeg føler at isen er brudt.

"Njah, de er okay," siger Johanne med et lille smil. "Vil du ikke fortælle mig lidt om din ikke-sådan-rigtig-kæreste?"

Vi sidder længe og snakker og drikker vores øl og holder hinanden i hånden. Hendes hænder er varme og stærke og samtidig er fingrene tynde og feminine. Jeg kommer til at tænke på hendes hænder mod andre dele af min krop og jeg rødmer. Hun ser det og smiler skævt og undlader at spørge.

"Må jeg invitere dig ud at spise?" spørger jeg, denne gang mindre usikkert, for nu tror jeg måske nok hun vil sige ja. "Eller har du andre planer?"

"Det må du da gerne!" siger Johanne med et smil. "Jeg har ikke andre planer. Det ville være dejligt!"

"Vi kan vist nok spise her på hotellet," siger jeg, "men der er også en anden restaurant nede på torvet, som ser bedre ud. Kender du den?"

"Jah... den der... det er vist et brasseri eller sådan noget? De laver dejlig mad!"

"Ja, den. Lad os gå derhen så... Eller er det for tidligt?"

"Njah... jeg er bare ikke så lækker efter vandreturen," siger Johanne. "Til at gå på restaurant, tænker jeg. Det er du vel heller ikke?"

"Næh, det er selvfølgelig rigtig nok, det havde jeg lige glemt. Vil du med op på mit værelse, så kan vi skiftes til at tage et bad?"

"Nåh ja, du har jo simpelthen et bad lige her," siger Johanne. "Smart. Lad os det!"

Så rejser vi os, og jeg tager Johanne i hånden og vi går op ad trappen til tredje sal, hvor mit værelse er.
