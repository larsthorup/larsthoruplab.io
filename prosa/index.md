---
layout: page-en
permalink: "/prosa/"
title: Prosa
---

# Prosastykker

### 2024

- [Når man går to på fjeldet](/prosa/2024/12/24/)
- [Frej og Lilja](/prosa/2024/09/29/)
- [Morgenrødme](/prosa/2024/09/25/morgen/)
- [Hvis du vil noget med litteratur!](/prosa/2024/09/25/litteratur/)
- [Skovbrynet](/prosa/2024/09/05/)
- [Svend](/prosa/2024/09/04/)

### 2023

- [Radioavisen](/prosa/2023/11/14/)
- [I toget](/prosa/2023/11/07/)

### 2021

- [Johanne](/prosa/2021/07/25/)