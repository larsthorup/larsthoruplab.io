# Cloudflare configuration

## Nameservers

* Follow Cloudflare instructions

## SSL

* Flexible
* Always use HTTPS

## DNS

* CNAME www - www.larsthorup.dk.s3-website.eu-central-1.amazonaws.com
