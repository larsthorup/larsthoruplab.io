---
date: 2024-08-28
layout: page-en
title: Om at skrive
author: Lars
permahost: "https://www.larsthorup.dk"
permalink: "/blog/2024/08/28/om-at-skrive/"
---

# Om at skrive

Lars Thorup, 2024-08-028

Jeg skriver det jeg tænker. Det jeg forestiller mig. Det jeg føler. Jeg skriver ikke "om" det jeg tænker, men skriver "det jeg tænker". Det falder mig nemt at beskrive en stemning, et landskab, en anden person, en scene. Replikker falder mig naturligt. Men det er jo bare fragmenter.

Men der mangler en rød tråd, en progression, en spænding, et plot. For mig føles livet ikke tilrettelagt, men som noget der sker. Jeg tænker aldrig i plot. Jeg har altid haft svært ved at tænke strategisk. Hvordan får jeg skabt en struktur på min fortælling? Hvordan kæder jeg alle de mange fragmenter sammen?

Og så er jeg frustreret over at så mange historier fanger læseren ind med negativ spænding: konflikter, vold, hemmeligheder, lidelse, sygdom, had. Det må være muligt at skabe noget lige så fængende ved hjælp af positiv spænding: håb, skønhed, kærlighed, optimisme, gå-på-mod. Men hvordan gør man det uden at det bliver pladder-romantisk, en lægeroman. Findes der pladder-romantisk litteratur? Eller hvis det faktisk er nødvendigt at have negativ spænding for at historien fænger, hvordan kan jeg så tilegne mig min egen måde at indarbejde negativ spænding uden at blive så frustreret at jeg mister lysten til at skrive?