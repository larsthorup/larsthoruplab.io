---
layout: page-en
permalink: "/blog/"
title: Blog
---

# Lars Thorups personlige blog

### 2024

- [Om at skrive](/blog/2024/08/28/om-at-skrive/)

### 2023

- [Tanker om et mentalt immunforsvar](/blog/2023/05/27/mentalt-immunforsvar/)
- [Udviklingen går ikke hurtigere og hurtigere](/blog/2023/04/07/udviklingens-hastighed/)