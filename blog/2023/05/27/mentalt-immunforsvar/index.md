---
date: 2023-05-27
layout: page-en
title: Tanker om et mentalt immunforsvar, første revision
author: Lars
permahost: "https://www.larsthorup.dk"
permalink: "/blog/2023/05/27/mentalt-immunforsvar/"
---

# Tanker om et mentalt immunforsvar

Skrevet af Lars Thorup, 7. juli 2023, lars@zealake.com, første udgave 27. maj 2023

- Har mennesker et *mentalt* immunforsvar? 
- Bliver alle mennesker hyppigt udsat for *belastende stimuli*, som fx ekstreme sanseindtryk, traumatiske erindringer, ubehagelige følelser og negative tanker? 
- Hjælper vores mentale immunforsvar os med at *undertrykke* sådanne belastende stimuli, fx ved at dæmpe, glemme eller fortrænge dem?
- Kan psykiske tilstande som angst, depression, mani i nogle tilfælde forklares med et *svækket* eller *overreagerende* mentalt immunforsvar?
- Kan nogle *autistiske* egenskaber forklares med et svækket eller overreagerende mentalt immunforsvar?
- Er såkaldt *psykosomatiske* symptomer snarere virkelige symptomer som et sundt mentalt immunforsvar ville have undertrykt?
- Hvordan *træner* man sit mentale immunforsvar?

## Hypotese om *det mentale immunforsvar*

Vores biologiske [immunforsvar](https://da.wikipedia.org/wiki/Immunforsvar) beskytter os mod belastende biokemiske stimuli:

- Skadelige molekyler, som fx proteiner, bekæmpes af antistoffer
- Virus bekæmpes af dræberceller
- Fremmede mikroorganismer, som fx bakterier, bekæmpes af makrofager
- Syge celler, som fx kræftceller, bekæmpes af hvide blodlegemer

Min tanke er, at vi på samme måde alle sammen har et *mentalt* immunforsvar, som filtrerer belastende mentale stimuli:

- Sansninger, som fx ubehagelige lyde, lys, berøring, smag, lugt
- Erindringer, som fx traumatiske oplevelser
- Følelser, som fx voldsom frygt, bekymring, mindreværd
- Tanker, som fx overvejelser om at gøre skade på sig selv eller andre

Jeg forestiller mig at dette mentale *filter* kan ses som et mentalt immunforsvar, fordi det undertrykker belastende stimuli fra at overvælde sindet. Hos mange mennesker fungerer dette mentale immunforsvar formentlig godt. Hvis man er særligt optimistisk anlagt, er det mentale immunforsvar måske særligt effektivt. Man kan også sammenligne det mentale immunforsvar med mental "integritet" - en slags mentalt forsvarsværk, der ikke så let lader belastende stimuli komme ind hvor de kan skabe usikkerhed og stress.

Hos mennesker med angst eller depression forestiller jeg mig at dette mentale immunforsvar fungerer dårligt. Man kan måske tale om at disse belastende mentale stimuli giver "mental overstimulering", "mentale udslæt" eller "mental høfeber". At de trætter, nedtrykker og ængster personer, hvis mentale immunforsvar er svækket, og at disse stimuli ender med at tage en stor del af sindets opmærksomhed, og dermed giver mindre plads til konstruktive stimuli.

Hos mennesker med OCD eller maniske træk kan man måske omvendt tale om at det mentale immunforsvar "overreagerer", som en slags allergisk reaktion, hvor personen ender med at være overopmærksom på alle også ikke-belastende mentale stimuli. 

For personer med autistiske egenskaber ser man en overhyppighed af angst og depression. Jeg forestiller mig at de mentale "filtre" harmonerer dårligt med visse autisters behov for at kende, forudsige, planlægge, afværge hvad der skal ske om lidt eller senere, for jo flere stimuli jo bedre mulighed for at planlægge. Måske er man som autist lykkedes med at dæmpe sit mentale immunforsvar for at kunne planlægge bedre?


## Hvad betyder *psykosomatisk*?

Psykiatere omtaler sommetider patienters kropslige symptomer som [(psyko-)somatiske](https://da.wikipedia.org/wiki/Psykosomatik). Det kan fx gælde når patienten oplever kropsligt ubehag som kløe eller smerte. Lægen kan så vurdere at disse symptomer er "psykosomatiske", dvs ikke er "reelle", men "indbildte". Man hører næsten formuleringer i stil med: "Vores målinger viser ikke at der er noget fysisk galt, så det er nok bare noget der foregår i dit hoved."

Denne sprogbrug får ofte patienten til at føle at det er vedkommendes egen skyld. Det lyder som om man burde kunne holde op med at bilde sig den slags ting ind. 

Men hvis angst og depression er følger af et *svækket* mentalt immunforsvar, så er der vel snarere tale om at alle mennesker har disse (såkaldt psykosomatiske) symptomer. Altså at alle de ubehagelige stimuli faktisk er helt reelle, og at alle mennesker har disse former for kropsligt ubehag. Blot har raske personer med deres velfungerende mentale immunforsvar evnet at undertrykke disse stimuli i større eller mindre omfang. Man kan altså snarere se det mentale immunforsvar som en "psykosomatisk" mekanisme vi alle sammen har, ikke til at skabe symptomer men til at dæmpe stimuli. Og hos personer med angst og depression er denne nyttige psykosomatiske mekanisme altså svækket. 


## Hvordan hjælper man sit mentale immunforsvar?

Rusmidler som fx alkohol, nikotin, hash, psilocybin og MDMA virker erfaringsmæssigt (kortvarigt) lindrende mod mange psykiske lidelser, herunder også mod angst og depression. Det skyldes måske at rusmidler netop giver en form for kunstig støtte til det mentale immunforsvar: de dæmper bekymringer og frygt, man glemmer ubehagelige oplevelser og ignorerer negative tanker, mens man er påvirket.

Lidt mere spekulativt virker religiøs tro måske blandt andet ved at styrke de troendes mentale immunforsvar. Hvis skæbnen er i gudernes magt, kan jeg bede til dem om hjælp, og derefter i en vis grad ignorere problemerne i tillid til at guderne hørte min bøn. Også den katolske syndsforladelse kan ses som en metode til at hjælpe den troende med at glemme ubehagelige oplevelser.

Måske virker nogle af vores psykofarmaka også ved at styrke det mentale immunforsvar? Jeg kender ikke til nogen omtale af fx dopamin og serotonin, som passer ind i denne hypotese om et mentalt immunforsvar, men det kunne være interessant at undersøge nærmere.

Kognitiv terapi virker ikke for mig som om det forholder sig direkte til personens mentale immunforsvar. Men måske kan man sammenligne kognitiv terapi med "sensitivitets" behandling på linje med fx græsfrø-injektioner til allergikere, idet man forsøger at tale traumer igennem så længe at sindet tilsidst opøver en vis *immunitet* over for dette stimuli. 

Måske kan man se meta-kognitiv terapi som et forsøg på at hjælpe det mentale immunforsvar med en række kognitive "tricks" eller afledemanøvrer.

En anden form for sensitivitets træning af det mentale immunforsvar kunne være øvelser som anbefales inden for såkaldt [polyvagal teori](https://en.wikipedia.org/wiki/Polyvagal_theory). Disse øvelser handler bl.a. om bevidst at pendulere frem og tilbage mellem let stressende mentale stimuli, fx gennem lydterapi eller vinterbadning, og tilbage til en ikke-stressende tilstand.

Hvordan kunne man ellers træne eller styrke sit mentale immunforsvar?
- Hvordan kan man blive bedre til at svække sin følsomhed over for sanseindtryk?
- Hvordan kan man blive bedre til at glemme traumatiske erindringer?
- Hvordan kan man blive bedre til at sige pyt til bekymringer man alligevel ikke kan gøre noget ved? 
- Hvordan kan man blive bedre til at undertrykke negative tanker før de overhovedet når frem til bevistheden?
- Hvordan kan man træne sit fokus på positive stimuli og måske dermed opnå at sænke sit fokus på belastende stimuli?

Vi ved næppe om dette mentale immunforsvar er biokemisk funderet eller kognitivt. Måske begge dele? Måske påvirker det biokemiske og det mentale immunforsvar hinanden som det bl.a. antydes af moderne forskning i vores mikrobiom? Måske finder vi engang frem til nye biokemiske midler som kan hjælpe med at styrke det mentale immunforsvar uden de ulemper der følger med de gængse rusmidler? Måske finder vi måder at kombinere rusmidler med andre midler så vi kun får den lindrende effekt uden skadevirkninger? Måske finder vi en dag på kognitive tricks der endnu bedre kan hjælpe med at styrke det mentale immunforsvar?
