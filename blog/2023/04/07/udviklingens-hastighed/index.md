---
date: 2023-04-07
layout: page-en
title: Udviklingen går ikke hurtigere og hurtigere
author: Lars
permahost: "https://www.larsthorup.dk"
permalink: "/blog/2023/04/07/udviklingens-hastighed/"
---

# Udviklingen går ikke hurtigere og hurtigere

Lars Thorup, 2023-04-07

Min gode ven, [Kristian Dupont](https://kristiandupont.com/), skrev for nylig en blog post om generativ AI, [Strapped to a Rocket: The Impact of AI](https://kristiandupont.medium.com/strapped-to-a-rocket-the-impact-of-ai-f5f175d43f22). Han skriver, som mange andre jævnligt giver udtryk for, at udviklingen går hurtigere og hurtigere med nærmest eksponentiel hastighed.

Men jeg tror ikke at udviklingen er eksponentiel, jeg tror at den bare "føles" sådan for os der oplever den over tid.

For hvis udviklingen faktisk bare er lineær, men at din og min (og alle andres) hjernes evne til at fatte ting kun er *logaritmisk* (med alderen) - så vil det *for os* føles eksponentielt, selvom det kun er lineært. 

Det jeg godt kan lide ved at se det på den måde, er at man ikke behøver give op. Udviklingen går ikke hurtigere end den nogen sinde har gjort og de næste generationer skal nok klare det. 

Og endnu vigtigere, hvis problemet ikke er udviklingen, men vores egne hjerner, så er der noget man kan gøre selv! Nemlig konstant træne sin hjerne til at forstå og omfavne de ændringer der kommer - presse den logaritmiske begrænsning lidt længere op mod den lineære virkelighed...

Endda er der nylig forskning som viser at innovation går *langsommere* på det seneste, ifølge denne artikel fra [Economist](https://www.economist.com/science-and-technology/2023/01/04/papers-and-patents-are-becoming-less-disruptive).