---
layout: page-en
permalink: "/"
title: Forside
---

# Hjemmeside for Lars Thorup

- Blog [her på hjemmesiden](/blog/).
- Fotos på [Instagram](https://www.instagram.com/larsearth/).
- Gamle fotos på [Tumblr](https://larsthorup.tumblr.com/).
- Musik jeg hører på [Spotify](https://open.spotify.com/user/lars.thorup).
- Chat via [Facebook Messenger](https://www.facebook.com/lars.thorup.5).
- Prosastykker [her på hjemmesiden](/prosa/).
- Arbejdsrelaterede aktiviter via [mit CV](https://www.fullstackagile.eu/larsthorup/).


<img src="./media/lars-2020-12-02-600px.jpg" width="38%"/>

